
<!--Gallery-->
	<div class="gallery py-5">
		<div class="container py-sm-3">
		<h2 class="heading text-capitalize mb-sm-5 mb-3"> Gallery </h2>
					<div class="row gallery-grids">
                                                
                                                <div class="col-lg-3 col-md-4 col-sm-6 ggd baner-top small wow fadeInLeft animated" data-wow-delay=".5s">
                                                    <a href="<?= Yii::getAlias('@web') ?>/images/s1.jpg" class="b-link-stripe b-animate-go  swipebox">
								<div class="gal-spin-effect vertical ">
									<img src="<?= Yii::getAlias('@web') ?>/images/s1.jpg" alt=" " />
									<div class="gal-text-box">
										<div class="info-gal-con">
											<h4>Intrend Gallery</h4>
											<span class="separator"></span>
											<p>Sit accusamus, vel blanditiis.</p>
											<span class="separator"></span>
											
										</div>
									</div>
								</div>
							</a>
						</div>
                                            
                                            
                                            
						<div class="col-lg-3 col-md-4 col-sm-6 ggd baner-top small wow fadeInLeft animated" data-wow-delay=".5s">
                                                    <a href="<?= Yii::getAlias('@web') ?>/images/s1.jpg" class="b-link-stripe b-animate-go  swipebox">
								<div class="gal-spin-effect vertical ">
									<img src="<?= Yii::getAlias('@web') ?>/images/s1.jpg" alt=" " />
									<div class="gal-text-box">
										<div class="info-gal-con">
											<h4>Intrend Gallery</h4>
											<span class="separator"></span>
											<p>Sit accusamus, vel blanditiis.</p>
											<span class="separator"></span>
											
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-lg-3 col-md-4 col-sm-6 ggd baner-top small wow fadeInLeft animated" data-wow-delay=".5s">
							<a href="images/s2.jpg" class="b-link-stripe b-animate-go  swipebox">
								<div class="gal-spin-effect vertical ">
									<img src="images/s2.jpg" alt=" " />
									<div class="gal-text-box">
										<div class="info-gal-con">
											<h4>Intrend Gallery</h4>
											<span class="separator"></span>
											<p>Sit accusamus, vel blanditiis.</p>
											<span class="separator"></span>
											
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-lg-3 col-md-4 col-sm-6 ggd baner-top small wow fadeInLeft animated" data-wow-delay=".5s">
							<a href="images/s3.jpg" class="b-link-stripe b-animate-go  swipebox">
								<div class="gal-spin-effect vertical ">
									<img src="images/s3.jpg" alt=" " />
									<div class="gal-text-box">
										<div class="info-gal-con">
											<h4>Intrend Gallery</h4>
											<span class="separator"></span>
											<p>Sit accusamus, vel blanditiis.</p>
											<span class="separator"></span>
											
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-lg-3 col-md-4 col-sm-6 ggd baner-top small wow fadeInUp animated" data-wow-delay=".5s">
							<a href="images/s4.jpg" class="b-link-stripe b-animate-go  swipebox">
								<div class="gal-spin-effect vertical ">
									<img src="images/s4.jpg" alt=" " />
									<div class="gal-text-box">
										<div class="info-gal-con">
											<h4>Intrend Gallery</h4>
											<span class="separator"></span>
											<p>Sit accusamus, vel blanditiis.</p>
											<span class="separator"></span>
											
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-lg-3 col-md-4 col-sm-6 ggd baner-top small ban-mar wow fadeInUp animated" data-wow-delay=".5s">
							<a href="images/s3.jpg" class="b-link-stripe b-animate-go  swipebox">
								<div class="gal-spin-effect vertical ">
									<img src="images/s3.jpg" alt=" " />
									<div class="gal-text-box">
										<div class="info-gal-con">
											<h4>Intrend Gallery</h4>
											<span class="separator"></span>
											<p>Sit accusamus, vel blanditiis.</p>
											<span class="separator"></span>
											
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-lg-3 col-md-4 col-sm-6 ggd baner-top small ban-mar wow fadeInUp animated" data-wow-delay=".5s">
							<a href="images/s1.jpg" class="b-link-stripe b-animate-go  swipebox">
								<div class="gal-spin-effect vertical ">
									<img src="images/s1.jpg" alt=" " />
									<div class="gal-text-box">
										<div class="info-gal-con">
											<h4>Intrend Gallery</h4>
											<span class="separator"></span>
											<p>Sit accusamus, vel blanditiis.</p>
											<span class="separator"></span>
											
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-lg-3 col-md-4 col-sm-6 ggd baner-top three wow ban-mar fadeInLeft animated gap-w3" data-wow-delay=".5s">
							<a href="images/s4.jpg" class="b-link-stripe b-animate-go  swipebox">
								<div class="gal-spin-effect vertical ">
									<img src="images/s4.jpg" alt=" " />
									<div class="gal-text-box">
										<div class="info-gal-con">
											<h4>Intrend Gallery</h4>
											<span class="separator"></span>
											<p>Sit accusamus, vel blanditiis.</p>
											<span class="separator"></span>
											
										</div>
									</div>
								</div>
							</a>
					</div>
					<div class="col-lg-3 col-md-4 col-sm-6 ggd baner-top three wow ban-mar fadeInLeft animated" data-wow-delay=".5s">
							<a href="images/s2.jpg" class="b-link-stripe b-animate-go  swipebox">
								<div class="gal-spin-effect vertical ">
									<img src="images/s2.jpg" alt=" " />
									<div class="gal-text-box">
										<div class="info-gal-con">
											<h4>Intrend Gallery</h4>
											<span class="separator"></span>
											<p>Sit accusamus, vel blanditiis.</p>
											<span class="separator"></span>
											
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-lg-3 col-md-4 col-sm-6 ggd baner-top three thre ban-mar wow fadeInLeft animated" data-wow-delay=".5s">
							<a href="images/s2.jpg" class="b-link-stripe b-animate-go  swipebox">
								<div class="gal-spin-effect vertical ">
									<img src="images/s2.jpg" alt=" " />
									<div class="gal-text-box">
										<div class="info-gal-con">
											<h4>Intrend Gallery</h4>
											<span class="separator"></span>
											<p>Sit accusamus, vel blanditiis.</p>
											<span class="separator"></span>
											
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-lg-3 col-md-4 col-sm-6 ggd baner-top three thre ban-mar wow fadeInLeft animated" data-wow-delay=".5s">
							<a href="images/s3.jpg" class="b-link-stripe b-animate-go  swipebox">
								<div class="gal-spin-effect vertical ">
									<img src="images/s3.jpg" alt=" " />
									<div class="gal-text-box">
										<div class="info-gal-con">
											<h4>Intrend Gallery</h4>
											<span class="separator"></span>
											<p>Sit accusamus, vel blanditiis.</p>
											<span class="separator"></span>
											
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-lg-3 col-md-4 col-sm-6 ggd baner-top three thre ban-mar wow fadeInLeft animated" data-wow-delay=".5s">
							<a href="images/s1.jpg" class="b-link-stripe b-animate-go  swipebox">
								<div class="gal-spin-effect vertical ">
									<img src="images/s1.jpg" alt=" " />
									<div class="gal-text-box">
										<div class="info-gal-con">
											<h4>Intrend Gallery</h4>
											<span class="separator"></span>
											<p>Sit accusamus, vel blanditiis.</p>
											<span class="separator"></span>
											
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-lg-3 col-md-4 col-sm-6 ggd baner-top three thre ban-mar wow fadeInLeft animated" data-wow-delay=".5s">
							<a href="images/s4.jpg" class="b-link-stripe b-animate-go  swipebox">
								<div class="gal-spin-effect vertical ">
									<img src="images/s4.jpg" alt=" " />
									<div class="gal-text-box">
										<div class="info-gal-con">
											<h4>Intrend Gallery</h4>
											<span class="separator"></span>
											<p>Sit accusamus, vel blanditiis.</p>
											<span class="separator"></span>
											
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>
				</div>
		</div>
	<!-- //gallery -->
