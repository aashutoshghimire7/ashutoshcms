<?php 
$this->title = 'Bent Ray Technologies' 
        
?>

<style>
    .preva{background-color: #ccc;}
    .slick-prev{background-color: #ccc;}
    .slick-prev:hover{
        background-color: rgba(0, 0, 0, 0.9); color:#fff;
    }
     .slick-next:hover{
        background-color: rgba(0, 0, 0, 0.9); color:#fff;
    }
    .slick-prev:focus{
        background-color: rgba(0, 0, 0, 0.9); color:#fff;
    }
     .slick-next:focus{
        background-color: rgba(0, 0, 0, 0.9); color:#fff;
    }
    
    .nexta{
    position:absolute;
    right:0;
    top:50%;
    tranform:translateY(-50%);
    }
    
    .preva{
    position:absolute;
    left:0;
    top:50%;
    tranform:translateY(-50%);
    }
</style>

<div class='banner' id='home'>
    <div class='cd-radial-slider-wrapper'>
        
        
        <ul class="cd-radial-slider" data-radius1="60" data-radius2="1364" data-centerx1="110" data-centerx2="1290">
            
            
                                <?php
                                    $directory = Yii::getAlias('@webroot').'/admin/images/slider';
                                    //$filename = glob($directory."/*.jpg")[0];
                                    //print_r($filename);die;
                                    //$out = substr($filename,strlen($directory)+1);
                                    //print_r($out);die;
                                    $allFiles = glob($directory."/*.jpg");
                                    foreach ($allFiles as $id=>$filename){

                                    ?>
            
                                        
            
                                        <li class="<?= ($id==0)?'visible':(($id==1)?'next-slide':(($id==(count(allFiles)-1)) ?'prev-slide':'')) ?>"> 
                                            <div class="svg-wrapper">
                                                    <svg viewBox="0 0 1400 800">
                                                            <title>Animated SVG</title>
                                                            <defs>
                                                                    <clipPath id="cd-image-<?= $id+1 ?>">
                                                                            <circle id="cd-circle-<?= $id+1 ?>" cx="110" cy="400" r="1364"/>
                                                                    </clipPath>
                                                            </defs> 
                                                            <image height='800px' width="1400px" clip-path="url(#cd-image-<?= $id+1 ?>)" xlink:href="<?php echo Yii::getAlias('@web').'/admin/images/slider/'.substr($filename,strlen($directory)+1); ?>"></image>
                                                    </svg>
                                            </div> <!-- .svg-wrapper --> 
                                            <div class="cd-radial-slider-content">
                                                    <div class="wrapper">
                                                            <div class="text-center">
                                                                    <h2>Interior Architecture </h2>
                                                                    <h3> Furniture </h3>
                                                                    <a href="about" class="read">Read More<i class="fas fa-caret-right"></i></a>
                                                            </div>
                                                    </div>
                                            </div> <!-- .cd-radial-slider-content -->
                                    </li> 
            
            
                                    
                                <?php
                                }
                                ?>
           
			</ul> <!-- .cd-radial-slider --> 
			<ul class="cd-radial-slider-navigation">
				<li><a href="#0" class="next"><i class="fas fa-chevron-right"></i></a></li>
				<li><a href="#0" class="prev"><i class="fas fa-chevron-left"></i></a></li>
			</ul>
    </div>
    </div>

<!-- about -->
<section class="wthree-row py-5">
	<div class="container py-lg-5 py-3">
		<h3 class="heading text-capitalize mb-sm-5 mb-4"> About us </h3>
		<div class="row d-flex justify-content-center">
			<div class="card col-lg-3 col-md-6 border-0">
				<div class="card-body bg-light pl-0 pr-0 pt-0">
					<h5 class=" card-title titleleft">Dining Chairs</h5>
					<p class="card-text mb-3">Class aptent taciti sociosqu adis litora torquent per conubia nostra per inceptos himenaeos.</p>
					<a href="#ab-bot" class="btn scroll">View More</a>
				</div>
				<img class="card-img-top" src="<?= Yii::getAlias('@web') ?>/images/a1.jpg" alt="Card image cap">
			</div>
			<div class="card col-lg-3 col-md-6 border-0 mt-md-0 mt-5">
				<img class="card-img-top" src="<?= Yii::getAlias('@web') ?>/images/a2.jpg " alt="Card image cap ">
				<div class="card-body bg-light text-center">
					<h5 class="card-title pt-3">Office Chairs</h5>
					<p class="card-text mb-3 ">Class aptent taciti sociosqu per conubia nostra per inceptos ad himenaeos.</p>
					<a href="#ab-bot" class="btn scroll">View More</a>
				</div>
			</div>
			<div class="card col-lg-3 col-md-6 border-0 mt-lg-0 mt-5 ">
				<img class="card-img-top " src="<?= Yii::getAlias('@web') ?>/images/a3.jpg " alt="Card image cap ">
				<div class="card-body bg-light text-center">
					<h5 class="card-title pt-3">Home Chairs</h5>
					<p class="card-text mb-3 ">Class aptent taciti sociosqu per conubia nostra per inceptos ad himenaeos.</p>
					<a href="#ab-bot" class="btn scroll">View More</a>
				</div>
			</div>
			<div class="card col-lg-3 col-md-6 border-0 mt-lg-0 mt-5 text-right">
				<div class="card-body bg-light pl-0 pr-0 pt-0">
					<h5 class="card-title titleright">Architecture</h5>
					<p class="card-text mb-3">Class aptent taciti sociosqu adis litora torquent per conubia nostra per inceptos himenaeos.</p>
					<a href="#ab-bot" class="btn scroll">View More</a>
				</div>
				<img class="card-img-top " src="<?= Yii::getAlias('@web') ?>/images/a4.jpg " alt="Card image cap ">
			</div>
		</div>
	</div>
</section>
<!-- //about -->

<!-- why choose us -->
<section class="why">
	<div class="layer py-5">
	<div class="container py-3">
		<h3 class="heading text-capitalize mb-sm-5 mb-4"> Why Choose us </h3>
		<div class="row why-grids">
			<div class="col-lg-3 col-sm-6 why-grid1">
				<i class="fas icon fa-tags"></i>
				<h4>10 year Gurantee</h4>
				<p class="mb-lg-5 mb-4">taciti aptent</p>
				<a href="#"><i class="fas fa-long-arrow-alt-right"></i></a>
			</div>
			<div class="col-lg-3 col-sm-6 mt-sm-0 mt-5 why-grid1">
				<i class="fas icon fa-puzzle-piece"></i>
				<h4>Comfortable support</h4>
				<p class="mb-lg-5 mb-4">taciti aptent</p>
				<a href="#"><i class="fas fa-long-arrow-alt-right"></i></a>
			</div>
			<div class="col-lg-3 col-sm-6 mt-lg-0 mt-5 why-grid1">
				<i class="fab icon fa-angellist"></i>
				<h4>Quality In Furniture</h4>
				<p class="mb-lg-5 mb-4">taciti aptent</p>
				<a href="#"><i class="fas fa-long-arrow-alt-right"></i></a>
			</div>
			<div class="col-lg-3 col-sm-6 mt-lg-0 mt-5 why-grid1">
				<i class="fas icon fa-dollar-sign"></i>
				<h4>100% Money Back</h4>
				<p class="mb-lg-5 mb-4">taciti aptent</p>
				<a href="#"><i class="fas fa-long-arrow-alt-right"></i></a>
			</div>
		</div>
	</div>
	</div>
</section>
<!-- //why choose us -->

<!-- team -->
<section class="w3ls-team py-5">
	<div class="container py-3">
		<h3 class="heading text-capitalize mb-sm-5 mb-4"> Our Team </h3>
		<div class="row team-grids">
                    
                    <?php
                    foreach ($teammember as $pep)
                    {
                    ?>    
                    
                    
			<div class="col-md-3 col-sm-6 w3_agileits-team1">
				<img class="img-fluid" src="<?= Yii::getAlias('@web/admin/images/people/').$pep->photo ?>" alt="">
				<h5 class="mt-3"><?= $pep->name ?></h5>
				<p><?= $pep->position ?></p>
				<div class="social-icons mt-2">
					<ul>
						<li>
							<a href="<?= ($pep->facebook)?('https://'.$pep->facebook):('#') ?>" class="fab fa-facebook-f icon-border facebook"> </a>
						</li>
						<li class="mx-1">
							<a href="<?= ($pep->twitter)?('https://'.$pep->twitter):('#') ?>" class="fab fa-twitter  icon-border twitter"> </a>
						</li>
						<li>
							<a href="<?= ($pep->googleplus)?('https://'.$pep->googleplus):('#') ?>" class="fab fa-google-plus-g icon-border googleplus"> </a>
						</li>
					</ul>
				</div>
			</div>
                    
                    <?php
                    }
                    ?>
                    
			
		</div>
	</div>
    <div class="nexta fas fa-chevron-right"></div>
                    <div class="preva fas fa-chevron-left"></div>
</section>
<!-- //team -->

<!-- Clients -->
<section class="clients-main">
	<div class="wthree-different-dot1 py-5">
		<div class="container py-sm-3">
			<!-- Owl-Carousel -->
			<h3 class="heading text-capitalize mb-sm-5 mb-4">What People Say </h3>
			<div class="cli-ent">
				<section class="slider">
					<div class="flexslider">
						<ul class="slides">
                                                    
                                                    <?php
                                                    foreach ($people as $pep)
                                                    {
                                                    ?>
                                                    
                                                    
							<li>
								<div class="item g1">
									<div class="agile-dish-caption">
									<img class="lazyOwl" src="<?= Yii::getAlias('@web/admin/images/people/').$pep->photo ?>" alt="" />
										<h5><?= $pep->name ?></h5>
										<h4><?= $pep->subject?></h4>
									</div>
									
									<div class="clearfix"></div>
									<p class="para-w3-agile"><span class="fa fa-quote-left" aria-hidden="true"></span> <?= $pep->description ?></p>
								</div>
							</li>
                                                        
                                                    <?php
                                                    }
                                                    ?>

						</ul>
					</div>
				</section>
			</div>
			<!--// Owl-Carousel -->
		</div>
	</div>
</section>
<!--// Clients -->

<!-- latest news -->
<div class="news py-5 my-sm-3">
	<div class="container">
		<h3 class="heading text-capitalize mb-sm-5 mb-4"> Latest News </h3>
		<div class="row news-grids">
			<div class="col-md-4 newsgrid1 text-center">
				<img src="<?= Yii::getAlias('@web') ?>/images/1.jpg" alt="news image" class="img-fluid" />
				<h4 class=" mt-4 text-uppercase">Interior Design Tips</h4>
				<p class="mt-4"> Praesent in congue leo, et rutrum justo. Integer porta nulla eu lorem.</p>
				<a href="single.html">View Post</a>
			</div>
			<div class="col-md-4 mt-md-0 mt-5 newsgrid1 text-center">
				<img src="<?= Yii::getAlias('@web') ?>/images/2.jpg" alt="news image" class="img-fluid" />
				<h4 class=" mt-4 text-uppercase">Interior Design Tips</h4>
				<p class="mt-4"> Praesent in congue leo, et rutrum justo. Integer porta nulla eu lorem.</p>
				<a href="single.html">View Post</a>
			</div>
			<div class="col-md-4 mt-md-0 mt-5 newsgrid1 text-center">
				<img src="<?= Yii::getAlias('@web') ?>/images/3.jpg" alt="news image" class="img-fluid" />
				<h4 class=" mt-4 text-uppercase">Interior Design Tips</h4>
				<p class="mt-4"> Praesent in congue leo, et rutrum justo. Integer porta nulla eu lorem.</p>
				<a href="single.html">View Post</a>
			</div>
		</div>
	</div>
</div>


<!-- //latest news -->

<!-- footer -->

<!-- footer -->

<!-- js-scripts -->
<?php
$slidesToShow = count($teammember)>3?'4':strval(count($teammember));
$slidesToScroll = count($teammember)>3?'4':strval(count($teammember));
$script = <<< JS
$('.row.team-grids').slick({
  dots: false,
  infinite: true,
  //speed: 300,
  slidesToShow: {$slidesToShow},
  slidesToScroll: {$slidesToScroll},
  //centerMode: true,
  //variableWidth: false,
  //adaptiveHeight: false
  nextArrow:$('.nexta'),
  prevArrow:$('.preva'),
});
JS;
$this->registerJs($script);
?>


