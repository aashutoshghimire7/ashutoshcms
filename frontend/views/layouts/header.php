<?php
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use common\models\Menu;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<header>
	<div class="container agile-banner_nav">
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			
                        <img src="<?= Yii::$app->request->baseUrl.'/images/logo.png'?>" style="width:90px">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent">
				<ul class="navbar-nav ml-auto">
                                        <?php
                                            NavBar::begin([
                                                
                                                'options' => [
                                                    'class' => 'navbar navbar-expand-lg navbar-light bg-light',
                                                    
                                                ],
                                            ]);
                                            $menuItems = [
                                                ['label' => 'Home', 'url' => ['/site/index']],
                                                ['label' => 'About', 'url' => ['/site/about']],
                                                ['label' => 'Contact', 'url' => ['/site/contact']],
                                            ];
                                            $menuItems = array_merge($menuItems, Menu::get('primary'));

                                            if (Yii::$app->user->isGuest) {
                                                $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
                                                $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
                                            } else {
                                                $menuItems[] = ['label' => "<span onclick=\"$('#check').submit()\">Logout(".Yii::$app->user->identity->username.")</span>"
                                         , 'url' =>[]
                                                    ];
                                                    ActiveForm::begin(['action'=>['/site/logout'],'id'=>'check','method'=>'post']);
                                                    Html::submitButton(
                                                        ['class' => 'btn btn-link logout']
                                                    );
                                                    ActiveForm::end();
                                            }
                                            echo Nav::widget([
                                                'options' => ['class' => 'navbar navbar-expand-lg navbar-light bg-light'],
                                                'items' => $menuItems,

                                                'encodeLabels' => false,
                                            ]);
                                            NavBar::end();
                                            ?>
					
				</ul>
			</div>
		  
		</nav>
	</div>
</header>
