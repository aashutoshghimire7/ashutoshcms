<?php

use common\models\Option;
use yii\helpers\Html;


?>
<div class="single post-view">

                <h1 class="entry-title"><?= Html::encode($post->title) ?></h1>


        <div class="entry-content clearfix">
            <?= $post->content ?>
        </div>
       