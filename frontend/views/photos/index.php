<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PhotosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Photos';
$this->params['breadcrumbs'][] = $this->title;
?>

<!--Gallery-->
	<div class="gallery py-5">
		<div class="container py-sm-3">
		<h2 class="heading text-capitalize mb-sm-5 mb-3"> <?= Html::encode($this->title) ?> </h2>
					<div class="row gallery-grids">
                                                
                                                
                                            <?php

                                                foreach($images as $img){

                                                ?>
                                                <div class="col-lg-3 col-md-4 col-sm-6 ggd baner-top three wow ban-mar fadeInLeft animated" data-wow-delay=".5s">
                                                    <div class="b-link-stripe b-animate-go  swipebox">
                                                        <div class="gal-spin-effect vertical ">
                                                            <img src=" <?php echo Yii::getAlias('@web').'/admin/uploads/'.$img->photofile; ?>" class="card-mg-top" width="90%">&nbsp;
                                                            <div href=" <?php echo Yii::getAlias('@web').'/admin/uploads/'.$img->photofile; ?>" class="gal-text-box" >
										<div class="info-gal-con">
											<h4>Gallery</h4>
											<span class="separator"></span>
											<p>Sit accusamus, vel blanditiis.</p>
											<span class="separator"></span>
                                                                                        <div class="text-bottom" style='display:none; position: absolute; top: 110%; left: 50%; transform: translate(-50%, -50%);'>
                                                                                            <a download href='<?= Yii::getAlias('@web').'/admin/uploads/'.$img->photofile ?>'><i class="fa fa-download" aria-hidden="true"></i></a>

                                                                                        </div>
											
										</div>
                                                            </div>
                                                          
                                                            
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                                }
                                                ?>
                                            
	
					</div>
				</div>
		</div>
	<!-- //gallery -->

<?php

$code = <<<JS

$(document).ready(function() {
        
  $("gal-spin-effect vertical").click(function() {
    var images = $(this).find('img').attr('src');
    window.open(images)
  });
  $(".gal-text-box").mouseover(function() {
    $(this).find('.text-bottom').show();
  });

  $(".gal-text-box").mouseout(function() {
    $(this).find('.text-bottom').hide();
  });
        
  $('.gal-text-box').magnificPopup({
  gallery: {
      enabled: true
    },
  type: 'image'
  // other options
});      
        
});

JS;
$this->registerJs($code);
?>