<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    //public $publishPath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/slider.css',
        'css/site.css',
        'css/bootstrap.css',
    	'css/style.css', 
    	'css/fontawesome-all.css',
        'css/flexslider.css',
        'http://fonts.googleapis.com/css?family=Poiret+One&amp;subset=cyrillic,latin-ext',
        'css/magnific-popup.css',
        'css/slick.css',
        //'css/slick-theme.css',
    ];
    public $js = [
    //'js/jquery-2.2.3.min.js',
	'js/bootstrap.js', 
	'js/snap.svg-min.js',
	'js/main.js',  
	'js/jquery.flexslider.js',
    'js/SmoothScroll.min.js',
	'js/move-top.js',
	'js/easing.js',
        'js/jquery.magnific-popup.js',
        'js/jquery.magnific-popup.min.js',
	'js/slick.js',
	'js/slick.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public function init(){
        //print_r(\Yii::getAlias('@webroot'));die;
    }
}
