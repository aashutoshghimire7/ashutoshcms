<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "wd_photos".
 *
 * @property integer $id
 * @property string $photofile
 */
class Photos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wd_photos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['photofile'], 'required'],
            [['photofile'], 'file', 'extensions'=>'jpg , png , jpeg , gif , bmp', 'maxFiles'=> 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'photofile' => 'Photofile',
        ];
    }
    
    public function getFilepath(){
        return Yii::getAlias('@webroot').'/uploads/'.$this->photofile;
    }
    
}
