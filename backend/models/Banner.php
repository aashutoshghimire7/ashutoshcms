<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "wd_banner".
 *
 * @property integer $id
 * @property string $slide
 */
class Banner extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wd_banner';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['slide'], 'required'],
            [['slide'], 'file', 'extensions'=>'jpg , png , jpeg , gif , bmp', 'maxFiles'=> 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'slide' => 'slide',
        ];
    }
    
    public function getFilepath(){
        return Yii::getAlias('@webroot').'/images/slider/'.$this->slide;
    }
    
}
