<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "wd_peoplesay".
 *
 * @property integer $id
 * @property string $photo
 * @property string $name
 * @property string $company_name
 * @property string $subject
 * @property string $description
 * @property integer $team
 * @property integer $facebook
 * @property integer $twitter
 * @property integer $googleplus
 * @property integer $position
 */
class Peoplesay extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wd_peoplesay';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['photo', 'position','name', 'company_name', 'subject', 'team'], 'required'],
            [['team'], 'integer'],
            [['description', 'facebook', 'twitter', 'googleplus'], 'string'],
            [['photo'], 'file', 'extensions'=>'jpg , png , jpeg , gif , bmp'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'photo' => 'Photo',
            'name' => 'Name',
            'company_name' => 'Company Name',
            'position' => 'position',
            'subject' => 'FeedBack',
            'description' => 'Description',
            'team' => 'Team',
            'facebook' => 'Facebook',
            'twitter' => 'Twitter',
            'googleplus' => 'Googleplus',
        ];
    }
}
