<?php

namespace backend\controllers;

use Yii;
use backend\models\Banner;
use backend\models\BannerSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * BannerController implements the CRUD actions for Banner model.
 */
class BannerController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Banner models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BannerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $data = Banner::find()->all();

        return $this->render('index', [
            'images'=>$data,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
        
        
    }

    /**
     * Displays a single Banner model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Banner model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Banner();

        if ($model->load(Yii::$app->request->post())) {
            $variablename = UploadedFile::getInstances($model, 'slide');
            //print_r($variablename);die;
            foreach($variablename as $pho)
            {
                $models = new Banner();
                $pho->saveAs('images/slider/' . $pho->baseName .''. date('Y-m-d').time() . '.' . $pho->extension);
                $pho = $pho->baseName .''. date('Y-m-d').time() .'.' . $pho->extension;
                $models->slide = $pho;
                if ($pho && $models->validate()) {                
                    $models->save();
                }
            }
            return $this->redirect(['index', 'id' => $model->id]);
            
        }
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        
    }

    /**
     * Updates an existing Banner model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Banner model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionDownload($id)
    {
        $data = Banner::findOne($id);
        header('Content-Type'.pathinfo($data->filepath,PATHINFO_EXTENSION));
        header('Content-Disposition: attachment; filename'.$data->filepath);
        return readfile($data->filepath);
    }
   public function actionRemove($id)
   {
       $data = Banner::findOne($id);
       unlink($data->filepath);
       $data->delete();

       return $this->redirect(['index']);
   }
    /**
     * Finds the Banner model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Banner the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Banner::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
