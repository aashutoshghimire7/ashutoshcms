<?php
/**
 * @link http://www.writesdown.com/
 * @author Agiel K. Saputra <13nightevil@gmail.com>
 * @copyright Copyright (c) 2015 WritesDown
 * @license http://www.writesdown.com/license/
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model common\models\Menu */
?>
<?php $form = ActiveForm::begin([
    'id' => 'create-menu-form',
    'action' => Url::to(['create']),
]) ?>

<div class="input-group">
    <div class="row">
        <div class="col-sm-6">
        <?= $form->field($model, 'title', ['template' => '{input}'])->textInput([
        'placeholder' => $model->getAttributeLabel('title'),
        ]) ?></div>
        
        <div class="col-sm-6">
            <?= Html::submitButton(Yii::t('writesdown', 'Add New Menu'), ['class' => 'btn btn-flat btn-primary']) ?>
        </div>
        
      </div>
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'location')->radioList(array('primary'=>'primary','secondary'=>'secondary'))->label(false); ?>
        </div>
      </div>
    

    
</div>
<?php ActiveForm::end() ?>
