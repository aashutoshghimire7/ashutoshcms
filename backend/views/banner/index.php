<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\BannerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Banner';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="Banner-index">

        <h1><?= Html::encode($this->title) ?></h1>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
           <?= Html::button('Create Banner', ['value'=>Url::toRoute(['banner/create']),'class' => 'btn btn-success', 'id'=>'modalButton']) ?>
        </p>

    <div class="row">
        <?php

        foreach($images as $img){

        ?>
        <div class="col-md-3">
            <div class="card" style="width:200px">
                <img src="<?php echo Yii::$app->request->baseUrl.'/images/slider/'.$img->slide; ?>" class="card-mg-top" width="100%">
                <div class="card-body">
                    <h5 class="card-title" style="word-wrap:break-word"> <?= $img->slide ?> </h5>
                    <div class="text-right">
                        <a download class='btn btn-primary' href='<?= Yii::getAlias('@web').'/images/slider/'.$img->slide ?>'>Download</a>
                        <?php
                        echo "&nbsp;";
                        echo Html::a('Delete', ['remove', 'id'=>$img->id],['class'=>'btn btn-danger']); 
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
        }
        ?>
    </div>
        
    <?php 
        Modal::begin([
            'header'=>'<h4>Posts</h4>',
            'id' => 'modal',
            'size' => 'modal-lg',
        ]);
        echo "<div id='modalContent'></div>";

        Modal::end();
    ?>
 
   
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],


            'slide',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>


<?php $script = <<< JS
$(document).ready(function(){
  $('#modalButton').click(function(){
        $('#modal').modal('show')
        .find('#modalContent')
        .load($(this).attr('value'));
    });
});
JS;
$this->registerJs($script);
?>

