<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Peoplesay */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="peoplesay-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'style'=>'width:50%']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'company_name')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'position')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'subject')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
    
    <img id="aImgShow" src="<?= ($model->photo)?Yii::getAlias('@web').'/images/people/'.$model->photo:'' ?>" class="carImage" width="100"/>
    
    <label class="control-label" for="peoplesay-photo">User Photo</label>
    
    <input type="file" name="Peoplesay[photo]" value="" onchange="uploadImage(this)" data-target="#aImgShow" />
    
    <div class="help-block"></div>

    <?= $form->field($model, 'team')->dropDownList(
        [1 => 'Yes', 0 => 'No'],['prompt'=>'Our Team Member?','style'=>'width:50%']
    ); ?>

    <?= $form->field($model, 'facebook')->textInput() ?>

    <?= $form->field($model, 'twitter')->textInput() ?>

    <?= $form->field($model, 'googleplus')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<script type="text/javascript">
         var imageTypes = ['jpeg', 'jpg', 'png']; //Validate the images to show
        function showImage(src, target)
        {
            var fr = new FileReader();
            fr.onload = function(e)
            {
                target.src = this.result;
            };
            fr.readAsDataURL(src.files[0]);

        }

        var uploadImage = function(obj)
        {
            var val = obj.value;
            var lastInd = val.lastIndexOf('.');
            var ext = val.slice(lastInd + 1, val.length);
            if (imageTypes.indexOf(ext) !== -1)
            {
                var id = $(obj).data('target');
                var src = obj;
                var target = $(id)[0];
                showImage(src, target);
            }
            else
            {

            }
        };
        
                 
</script>
