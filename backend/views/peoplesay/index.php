<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PeoplesaySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Peoplesays';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="peoplesay-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Peoplesay', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'photo',
            'name',
            'company_name',
            'subject',
            // 'description:ntext',
            // 'team',
            // 'facebook',
            // 'twitter',
            // 'googleplus',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
