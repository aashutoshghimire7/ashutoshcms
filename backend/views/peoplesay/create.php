<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Peoplesay */

$this->title = 'Create Peoplesay';
$this->params['breadcrumbs'][] = ['label' => 'Peoplesays', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="peoplesay-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
