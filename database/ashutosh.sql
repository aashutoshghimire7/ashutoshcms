-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 10, 2019 at 08:07 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ashutosh`
--

-- --------------------------------------------------------

--
-- Table structure for table `wd_auth_assignment`
--

CREATE TABLE `wd_auth_assignment` (
  `item_name` varchar(64) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wd_auth_assignment`
--

INSERT INTO `wd_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('superadmin', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wd_auth_item`
--

CREATE TABLE `wd_auth_item` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `rule_name` varchar(64) DEFAULT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wd_auth_item`
--

INSERT INTO `wd_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('administrator', 1, 'Administrator', NULL, NULL, 0, 0),
('author', 1, 'Author', NULL, NULL, 0, 0),
('contributor', 1, 'Contributor', NULL, NULL, 0, 0),
('editor', 1, 'Editor', NULL, NULL, 0, 0),
('subscriber', 1, 'Subscriber', NULL, NULL, 0, 0),
('superadmin', 1, 'Super Administrator', NULL, NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wd_auth_item_child`
--

CREATE TABLE `wd_auth_item_child` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wd_auth_item_child`
--

INSERT INTO `wd_auth_item_child` (`parent`, `child`) VALUES
('administrator', 'editor'),
('author', 'contributor'),
('contributor', 'subscriber'),
('editor', 'author'),
('superadmin', 'administrator');

-- --------------------------------------------------------

--
-- Table structure for table `wd_auth_rule`
--

CREATE TABLE `wd_auth_rule` (
  `name` varchar(64) NOT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wd_media`
--

CREATE TABLE `wd_media` (
  `id` int(11) NOT NULL,
  `author` int(11) NOT NULL,
  `post_id` int(11) DEFAULT NULL,
  `title` text NOT NULL,
  `excerpt` text,
  `content` text,
  `password` varchar(255) DEFAULT NULL,
  `date` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `slug` varchar(255) NOT NULL,
  `mime_type` varchar(100) NOT NULL,
  `comment_status` varchar(20) NOT NULL DEFAULT 'open',
  `comment_count` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wd_media`
--

INSERT INTO `wd_media` (`id`, `author`, `post_id`, `title`, `excerpt`, `content`, `password`, `date`, `modified`, `slug`, `mime_type`, `comment_status`, `comment_count`) VALUES
(1, 1, NULL, '1', NULL, NULL, NULL, '2019-01-10 12:23:41', '2019-01-10 12:23:41', '1', 'image/jpeg', 'open', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wd_media_comment`
--

CREATE TABLE `wd_media_comment` (
  `id` int(11) NOT NULL,
  `media_id` int(11) NOT NULL,
  `author` text,
  `email` varchar(100) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `ip` varchar(100) NOT NULL,
  `date` datetime NOT NULL,
  `content` text NOT NULL,
  `status` varchar(20) NOT NULL,
  `agent` varchar(255) NOT NULL,
  `parent` int(11) DEFAULT '0',
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wd_media_meta`
--

CREATE TABLE `wd_media_meta` (
  `id` int(11) NOT NULL,
  `media_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wd_media_meta`
--

INSERT INTO `wd_media_meta` (`id`, `media_id`, `name`, `value`) VALUES
(1, 1, 'metadata', '{\"filename\":\"1.jpg\",\"file_size\":20170,\"versions\":{\"full\":{\"url\":\"2019/01/1.jpg\",\"width\":300,\"height\":201},\"medium\":{\"url\":\"2019/01/1-300x201.jpg\",\"width\":300,\"height\":201},\"thumbnail\":{\"url\":\"2019/01/1-150x150.jpg\",\"width\":\"150\",\"height\":\"150\"}},\"icon_url\":\"2019/01/1-150x150.jpg\"}');

-- --------------------------------------------------------

--
-- Table structure for table `wd_menu`
--

CREATE TABLE `wd_menu` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `location` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wd_menu`
--

INSERT INTO `wd_menu` (`id`, `title`, `location`) VALUES
(4, 'header-menu', 'primary');

-- --------------------------------------------------------

--
-- Table structure for table `wd_menu_item`
--

CREATE TABLE `wd_menu_item` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `label` varchar(255) NOT NULL,
  `url` text NOT NULL,
  `description` text,
  `order` int(11) NOT NULL DEFAULT '0',
  `parent` int(11) NOT NULL DEFAULT '0',
  `options` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wd_menu_item`
--

INSERT INTO `wd_menu_item` (`id`, `menu_id`, `label`, `url`, `description`, `order`, `parent`, `options`) VALUES
(6, 4, 'Gallery', '/ashutosh/photos/', '', 0, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wd_migration`
--

CREATE TABLE `wd_migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wd_migration`
--

INSERT INTO `wd_migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1546601981),
('m000000_000001_option', 1546602359),
('m000000_000002_user', 1546602359),
('m000000_000003_auth_rule', 1546602359),
('m000000_000004_auth_item', 1546602359),
('m000000_000005_auth_item_child', 1546602360),
('m000000_000006_auth_assignment', 1546602360),
('m000000_000007_post_type', 1546602361),
('m000000_000008_taxonomy', 1546602361),
('m000000_000009_post_type_taxonomy', 1546602361),
('m000000_000010_term', 1546602361),
('m000000_000011_post', 1546602362),
('m000000_000012_term_relationship', 1546602362),
('m000000_000013_post_meta', 1546602362),
('m000000_000014_post_comment', 1546602363),
('m000000_000015_media', 1546602363),
('m000000_000016_media_meta', 1546602363),
('m000000_000017_media_comment', 1546602363),
('m000000_000018_menu', 1546602363),
('m000000_000019_menu_item', 1546602364),
('m000000_000020_module', 1546602364),
('m000000_000021_widget', 1546602364);

-- --------------------------------------------------------

--
-- Table structure for table `wd_module`
--

CREATE TABLE `wd_module` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `title` text NOT NULL,
  `description` text,
  `config` text NOT NULL,
  `status` smallint(1) NOT NULL DEFAULT '0',
  `directory` varchar(128) NOT NULL,
  `backend_bootstrap` smallint(1) NOT NULL DEFAULT '0',
  `frontend_bootstrap` smallint(1) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wd_module`
--

INSERT INTO `wd_module` (`id`, `name`, `title`, `description`, `config`, `status`, `directory`, `backend_bootstrap`, `frontend_bootstrap`, `date`, `modified`) VALUES
(1, 'toolbar', 'Toolbar', NULL, '{\"frontend\":{\"class\":\"modules\\\\toolbar\\\\frontend\\\\Module\"}}', 0, 'toolbar', 0, 1, '2015-09-11 03:14:57', '2015-09-11 03:14:57'),
(2, 'sitemap', 'Sitemap', 'Module for sitemap', '{\"backend\":{\"class\":\"modules\\\\sitemap\\\\backend\\\\Module\"},\"frontend\":{\"class\":\"modules\\\\sitemap\\\\frontend\\\\Module\"}}', 0, 'sitemap', 0, 1, '2015-09-11 03:38:25', '2015-09-11 03:38:25'),
(3, 'feed', 'RSS Feed', NULL, '{\"frontend\":{\"class\":\"modules\\\\feed\\\\frontend\\\\Module\"}}', 0, 'feed', 0, 0, '2015-09-11 03:38:53', '2015-09-11 03:38:53');

-- --------------------------------------------------------

--
-- Table structure for table `wd_option`
--

CREATE TABLE `wd_option` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `value` text NOT NULL,
  `label` varchar(64) DEFAULT NULL,
  `group` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wd_option`
--

INSERT INTO `wd_option` (`id`, `name`, `value`, `label`, `group`) VALUES
(1, 'sitetitle', 'WritesDown', 'Site Title', 'general'),
(2, 'tagline', 'CMS Built with Yii Framework', 'Tagline', 'general'),
(3, 'admin_email', 'superadmin@writesdown.com', 'E-mail Address', 'general'),
(4, 'allow_signup', '0', 'Membership', 'general'),
(5, 'default_role', 'subscriber', 'New User Default Role', 'general'),
(6, 'time_zone', 'Asia/Jakarta', 'Timezone', 'general'),
(7, 'date_format', 'F d, Y', 'Date Format', 'general'),
(8, 'time_format', 'g:i:s a', 'Time Format', 'general'),
(9, 'show_on_front', 'posts', 'Front page displays', 'reading'),
(10, 'front_post_type', 'all', 'Post type on front page', 'reading'),
(11, 'front_page', '', 'Front page', 'reading'),
(12, 'posts_page', '', 'Posts page', 'reading'),
(13, 'posts_per_page', '10', 'Posts Per Page', 'reading'),
(14, 'posts_per_rss', '10', 'Posts Per RSS', 'reading'),
(15, 'rss_use_excerpt', '0', 'For each article in a feed, show ', 'reading'),
(16, 'disable_site_indexing', '0', 'Search Engine Visibility ', 'reading'),
(17, 'default_comment_status', 'open', 'Default article settings', 'discussion'),
(18, 'require_name_email', '1', 'Comment author must fill out name and e-mail ', 'discussion'),
(19, 'comment_registration', '0', 'Users must be registered and logged in to comment ', 'discussion'),
(20, 'close_comments_for_old_posts', '0', 'Automatically close comments on articles older', 'discussion'),
(21, 'close_comments_days_old', '14', 'Days when the comments of the article is closed', 'discussion'),
(22, 'thread_comments', '1', 'Enable threaded (nested) comments', 'discussion'),
(23, 'page_comments', '5', 'Break comments into pages with', 'discussion'),
(24, 'thread_comments_depth', '5', 'Thread Comments Depth', 'discussion'),
(25, 'comments_per_page', '10', 'Top level comments per page', 'discussion'),
(26, 'default_comments_page', 'newest', 'page displayed by default\\nComments should be displayed with the', 'discussion'),
(27, 'comments_notify', '1', 'Notify when anyone posts a comment', 'discussion'),
(28, 'moderation_notify', '0', 'Notify when a comment is held for moderation', 'discussion'),
(29, 'comment_moderation', '1', 'Comment must be manually approved', 'discussion'),
(30, 'comment_whitelist', '0', 'Comment author must have a previously approved comment', 'discussion'),
(31, 'show_avatars', '1', 'Show Avatars', 'discussion'),
(32, 'avatar_rating', 'G', 'Maximum Rating', 'discussion'),
(33, 'avatar_default', 'mystery', 'Default Avatar', 'discussion'),
(34, 'comment_order', 'asc', 'comments at the top of each page', 'discussion'),
(35, 'thumbnail_width', '150', 'Width', 'media'),
(36, 'thumbnail_height', '150', 'Height', 'media'),
(37, 'thumbnail_crop', '1', 'Crop thumbnail to exact dimensions', 'media'),
(38, 'medium_width', '300', 'Max Width', 'media'),
(39, 'medium_height', '300', 'Max Height', 'media'),
(40, 'large_width', '1024', 'Max Width', 'media'),
(41, 'large_height', '1024', 'Max Height', 'media'),
(42, 'uploads_yearmonth_based', '1', 'Organize my uploads into month- and year-based folders', 'media'),
(43, 'uploads_username_based', '0', 'Organize my uploads into username-based folders', 'media'),
(44, 'theme', 'default', 'Theme', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wd_photos`
--

CREATE TABLE `wd_photos` (
  `id` int(11) NOT NULL,
  `photofile` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wd_photos`
--

INSERT INTO `wd_photos` (`id`, `photofile`) VALUES
(55, '1995-the-mgf-Image12332016833342019-01-101547100147.jpg'),
(56, '2001-a-new-range-Image12332016834142018-12-271545901288 (2)2019-01-101547100147.jpg'),
(57, '2010-mg6-Image1233201620642019-01-101547100903.jpg'),
(58, '2012-a-return-to-the-british-touring-car-championship-Image1233201620892018-12-2715459012882019-01-101547100903.jpg'),
(59, '2013-mg3-Image12332016209312019-01-101547100903.jpg'),
(60, '2015-mg-gs-Image123320162010552018-12-2715459012892019-01-101547100903.jpg'),
(61, '2017-mg-360-Image112102017192422019-01-101547100903.jpg'),
(62, 'blog2019-01-101547100903.jpg'),
(63, 'logo (1)2019-01-101547100903.png'),
(64, 'logo2019-01-101547100903.png'),
(65, 'Nepal-Flag-3-icon2019-01-101547100903.png');

-- --------------------------------------------------------

--
-- Table structure for table `wd_post`
--

CREATE TABLE `wd_post` (
  `id` int(11) NOT NULL,
  `author` int(11) NOT NULL,
  `type` int(11) DEFAULT NULL,
  `title` text NOT NULL,
  `excerpt` text,
  `content` text,
  `date` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'publish',
  `password` varchar(255) DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `comment_status` varchar(20) NOT NULL DEFAULT 'open',
  `comment_count` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wd_post`
--

INSERT INTO `wd_post` (`id`, `author`, `type`, `title`, `excerpt`, `content`, `date`, `modified`, `status`, `password`, `slug`, `comment_status`, `comment_count`) VALUES
(1, 1, 1, 'Sample Post', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p><p>In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet.</p><p>Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus.</p>', '2019-01-04 17:31:02', '2019-01-04 17:31:02', 'publish', NULL, 'sample-post', 'open', 1),
(2, 1, 2, 'Sample Page', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p><blockquote><p>In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet.</p></blockquote><p>Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus.</p>', '2019-01-04 17:31:02', '2019-01-04 17:31:02', 'publish', NULL, 'sample-page', 'close', 0),
(3, 1, 2, 'Testing page', 'Hey this is for test.', '<p>Hey this is for test.</p>', '2019-01-04 17:31:59', '2019-01-04 17:32:19', 'publish', NULL, 'testing-page', 'open', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wd_post_comment`
--

CREATE TABLE `wd_post_comment` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `author` text,
  `email` varchar(100) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `ip` varchar(100) NOT NULL,
  `date` datetime NOT NULL,
  `content` text NOT NULL,
  `status` varchar(20) NOT NULL,
  `agent` varchar(255) NOT NULL,
  `parent` int(11) DEFAULT '0',
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wd_post_comment`
--

INSERT INTO `wd_post_comment` (`id`, `post_id`, `author`, `email`, `url`, `ip`, `date`, `content`, `status`, `agent`, `parent`, `user_id`) VALUES
(1, 1, 'WD, WritesDown', 'wd@writesdown.com', 'http://www.writesdown.com/', '', '2019-01-04 17:31:03', 'SAMPLE COMMENT: Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris.', 'approved', '', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wd_post_meta`
--

CREATE TABLE `wd_post_meta` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wd_post_type`
--

CREATE TABLE `wd_post_type` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `slug` varchar(64) NOT NULL,
  `description` text,
  `icon` varchar(255) DEFAULT NULL,
  `singular_name` varchar(255) NOT NULL,
  `plural_name` varchar(255) NOT NULL,
  `menu_builder` smallint(1) NOT NULL DEFAULT '0',
  `permission` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wd_post_type`
--

INSERT INTO `wd_post_type` (`id`, `name`, `slug`, `description`, `icon`, `singular_name`, `plural_name`, `menu_builder`, `permission`) VALUES
(1, 'post', 'post', NULL, 'fa fa-thumb-tack', 'Post', 'Posts', 0, 'contributor'),
(2, 'page', 'page', NULL, 'fa fa-file-o', 'Page', 'Pages', 1, 'editor');

-- --------------------------------------------------------

--
-- Table structure for table `wd_post_type_taxonomy`
--

CREATE TABLE `wd_post_type_taxonomy` (
  `post_type_id` int(11) NOT NULL,
  `taxonomy_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wd_post_type_taxonomy`
--

INSERT INTO `wd_post_type_taxonomy` (`post_type_id`, `taxonomy_id`) VALUES
(1, 1),
(1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `wd_taxonomy`
--

CREATE TABLE `wd_taxonomy` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `hierarchical` smallint(1) NOT NULL DEFAULT '0',
  `singular_name` varchar(255) NOT NULL,
  `plural_name` varchar(255) NOT NULL,
  `menu_builder` smallint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wd_taxonomy`
--

INSERT INTO `wd_taxonomy` (`id`, `name`, `slug`, `hierarchical`, `singular_name`, `plural_name`, `menu_builder`) VALUES
(1, 'category', 'category', 1, 'Category', 'Categories', 1),
(2, 'tag', 'tag', 0, 'Tag', 'Tags', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wd_term`
--

CREATE TABLE `wd_term` (
  `id` int(11) NOT NULL,
  `taxonomy_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `description` text,
  `parent` int(11) DEFAULT '0',
  `count` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wd_term`
--

INSERT INTO `wd_term` (`id`, `taxonomy_id`, `name`, `slug`, `description`, `parent`, `count`) VALUES
(1, 1, 'Sample Category', 'sample-category', 'Hello there, this is example of category.', 0, 1),
(2, 2, 'Sample Tag', 'sample-tag', 'Hello there, this is an example of tag.', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `wd_term_relationship`
--

CREATE TABLE `wd_term_relationship` (
  `post_id` int(11) NOT NULL,
  `term_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wd_term_relationship`
--

INSERT INTO `wd_term_relationship` (`post_id`, `term_id`) VALUES
(1, 1),
(1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `wd_user`
--

CREATE TABLE `wd_user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `display_name` varchar(255) DEFAULT NULL,
  `password_hash` varchar(255) NOT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `auth_key` varchar(32) NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '5',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `login_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wd_user`
--

INSERT INTO `wd_user` (`id`, `username`, `email`, `full_name`, `display_name`, `password_hash`, `password_reset_token`, `auth_key`, `status`, `created_at`, `updated_at`, `login_at`) VALUES
(1, 'superadmin', 'superadministrator@writesdown.com', 'Super Administrator', 'Super Admin', '$2y$13$WJIxqq6WBZUw7tyfN2oiH.WJtPntvLMjs6NG9uW0M3Lh71lImaEyu', NULL, '7QvEmdZDvaSxM1-oYoWkKso0ws6AHTX1', 10, '2019-01-04 17:30:59', '2019-01-04 17:30:59', '2019-01-04 17:30:59');

-- --------------------------------------------------------

--
-- Table structure for table `wd_userf`
--

CREATE TABLE `wd_userf` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `display_name` varchar(255) DEFAULT NULL,
  `password_hash` varchar(255) NOT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `auth_key` varchar(32) NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '5',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `login_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wd_userf`
--

INSERT INTO `wd_userf` (`id`, `username`, `email`, `full_name`, `display_name`, `password_hash`, `password_reset_token`, `auth_key`, `status`, `created_at`, `updated_at`, `login_at`) VALUES
(1, 'superadmin', 'superadministrator@writesdown.com', 'Super Administrator', 'Super Admin', '$2y$13$WJIxqq6WBZUw7tyfN2oiH.WJtPntvLMjs6NG9uW0M3Lh71lImaEyu', NULL, '7QvEmdZDvaSxM1-oYoWkKso0ws6AHTX1', 10, '2019-01-04 17:30:59', '2019-01-04 17:30:59', '2019-01-04 17:30:59');

-- --------------------------------------------------------

--
-- Table structure for table `wd_widget`
--

CREATE TABLE `wd_widget` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `config` text NOT NULL,
  `location` varchar(128) NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0',
  `directory` varchar(128) NOT NULL,
  `date` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wd_auth_assignment`
--
ALTER TABLE `wd_auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `wd_auth_item`
--
ALTER TABLE `wd_auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`);

--
-- Indexes for table `wd_auth_item_child`
--
ALTER TABLE `wd_auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Indexes for table `wd_auth_rule`
--
ALTER TABLE `wd_auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `wd_media`
--
ALTER TABLE `wd_media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `author` (`author`);

--
-- Indexes for table `wd_media_comment`
--
ALTER TABLE `wd_media_comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `media_id` (`media_id`);

--
-- Indexes for table `wd_media_meta`
--
ALTER TABLE `wd_media_meta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `media_id` (`media_id`);

--
-- Indexes for table `wd_menu`
--
ALTER TABLE `wd_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wd_menu_item`
--
ALTER TABLE `wd_menu_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_id` (`menu_id`);

--
-- Indexes for table `wd_migration`
--
ALTER TABLE `wd_migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `wd_module`
--
ALTER TABLE `wd_module`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wd_option`
--
ALTER TABLE `wd_option`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wd_photos`
--
ALTER TABLE `wd_photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wd_post`
--
ALTER TABLE `wd_post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type` (`type`),
  ADD KEY `author` (`author`);

--
-- Indexes for table `wd_post_comment`
--
ALTER TABLE `wd_post_comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_id` (`post_id`);

--
-- Indexes for table `wd_post_meta`
--
ALTER TABLE `wd_post_meta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_id` (`post_id`);

--
-- Indexes for table `wd_post_type`
--
ALTER TABLE `wd_post_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wd_post_type_taxonomy`
--
ALTER TABLE `wd_post_type_taxonomy`
  ADD PRIMARY KEY (`post_type_id`,`taxonomy_id`),
  ADD KEY `taxonomy_id` (`taxonomy_id`);

--
-- Indexes for table `wd_taxonomy`
--
ALTER TABLE `wd_taxonomy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wd_term`
--
ALTER TABLE `wd_term`
  ADD PRIMARY KEY (`id`),
  ADD KEY `taxonomy_id` (`taxonomy_id`);

--
-- Indexes for table `wd_term_relationship`
--
ALTER TABLE `wd_term_relationship`
  ADD PRIMARY KEY (`post_id`,`term_id`),
  ADD KEY `term_id` (`term_id`);

--
-- Indexes for table `wd_user`
--
ALTER TABLE `wd_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wd_userf`
--
ALTER TABLE `wd_userf`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wd_widget`
--
ALTER TABLE `wd_widget`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wd_media`
--
ALTER TABLE `wd_media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wd_media_comment`
--
ALTER TABLE `wd_media_comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wd_media_meta`
--
ALTER TABLE `wd_media_meta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wd_menu`
--
ALTER TABLE `wd_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `wd_menu_item`
--
ALTER TABLE `wd_menu_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `wd_module`
--
ALTER TABLE `wd_module`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `wd_option`
--
ALTER TABLE `wd_option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `wd_photos`
--
ALTER TABLE `wd_photos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `wd_post`
--
ALTER TABLE `wd_post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `wd_post_comment`
--
ALTER TABLE `wd_post_comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wd_post_meta`
--
ALTER TABLE `wd_post_meta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wd_post_type`
--
ALTER TABLE `wd_post_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wd_taxonomy`
--
ALTER TABLE `wd_taxonomy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wd_term`
--
ALTER TABLE `wd_term`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wd_user`
--
ALTER TABLE `wd_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wd_userf`
--
ALTER TABLE `wd_userf`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wd_widget`
--
ALTER TABLE `wd_widget`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `wd_auth_assignment`
--
ALTER TABLE `wd_auth_assignment`
  ADD CONSTRAINT `wd_auth_assignment_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `wd_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `wd_auth_assignment_ibfk_2` FOREIGN KEY (`item_name`) REFERENCES `wd_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wd_auth_item`
--
ALTER TABLE `wd_auth_item`
  ADD CONSTRAINT `wd_auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `wd_auth_rule` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wd_auth_item_child`
--
ALTER TABLE `wd_auth_item_child`
  ADD CONSTRAINT `wd_auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `wd_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `wd_auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `wd_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wd_media`
--
ALTER TABLE `wd_media`
  ADD CONSTRAINT `wd_media_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `wd_post` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `wd_media_ibfk_2` FOREIGN KEY (`author`) REFERENCES `wd_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wd_media_comment`
--
ALTER TABLE `wd_media_comment`
  ADD CONSTRAINT `wd_media_comment_ibfk_1` FOREIGN KEY (`media_id`) REFERENCES `wd_media` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wd_media_meta`
--
ALTER TABLE `wd_media_meta`
  ADD CONSTRAINT `wd_media_meta_ibfk_1` FOREIGN KEY (`media_id`) REFERENCES `wd_media` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wd_menu_item`
--
ALTER TABLE `wd_menu_item`
  ADD CONSTRAINT `wd_menu_item_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `wd_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wd_post`
--
ALTER TABLE `wd_post`
  ADD CONSTRAINT `wd_post_ibfk_1` FOREIGN KEY (`type`) REFERENCES `wd_post_type` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `wd_post_ibfk_2` FOREIGN KEY (`author`) REFERENCES `wd_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wd_post_comment`
--
ALTER TABLE `wd_post_comment`
  ADD CONSTRAINT `wd_post_comment_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `wd_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wd_post_meta`
--
ALTER TABLE `wd_post_meta`
  ADD CONSTRAINT `wd_post_meta_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `wd_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wd_post_type_taxonomy`
--
ALTER TABLE `wd_post_type_taxonomy`
  ADD CONSTRAINT `wd_post_type_taxonomy_ibfk_1` FOREIGN KEY (`post_type_id`) REFERENCES `wd_post_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `wd_post_type_taxonomy_ibfk_2` FOREIGN KEY (`taxonomy_id`) REFERENCES `wd_taxonomy` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wd_term`
--
ALTER TABLE `wd_term`
  ADD CONSTRAINT `wd_term_ibfk_1` FOREIGN KEY (`taxonomy_id`) REFERENCES `wd_taxonomy` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wd_term_relationship`
--
ALTER TABLE `wd_term_relationship`
  ADD CONSTRAINT `wd_term_relationship_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `wd_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `wd_term_relationship_ibfk_2` FOREIGN KEY (`term_id`) REFERENCES `wd_term` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
